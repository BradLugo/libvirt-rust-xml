{
  inputs = {
    rust-overlay = {
      url = "github:oxalica/rust-overlay";
    };

    flake-utils.follows = "rust-overlay/flake-utils";
    nixpkgs.follows = "rust-overlay/nixpkgs";
  };

  outputs = inputs: with inputs;
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
          overlays = [ rust-overlay.overlays.default ];
        };
      in
      {
        devShells.default = pkgs.mkShell {
          shellHook = ''
            rustup default stable
          '';

          buildInputs = with pkgs; [
            rustup
          ];
        };
      });
}
