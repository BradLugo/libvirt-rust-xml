use serde::{Deserialize, Serialize};
use serde_with::skip_serializing_none;

#[derive(Debug, Serialize, Deserialize, PartialEq)]
#[serde(rename = "type")]
#[serde(rename_all = "lowercase")]
pub enum DomainControllerType {
    IDE,
    FDC,
    SCSI,
    SATA,
    USB,
    CCID,
    VirtioSerial,
    PCI,
}

#[derive(Debug, Serialize, Deserialize, PartialEq)]
#[serde(rename_all = "camelCase")]
pub struct DomainController {
    #[serde(rename = "type")]
    pub r#type: DomainControllerType,
    pub index: String,
}

#[derive(Debug, Serialize, Deserialize, PartialEq)]
#[serde(rename_all = "camelCase")]
pub struct DomainDiskFileSource {
    #[serde(rename = "@file")]
    pub file: String,
}

#[derive(Debug, Serialize, Deserialize, PartialEq)]
#[serde(rename_all = "camelCase")]
pub struct DomainDiskDriver {
    #[serde(rename = "@type")]
    pub r#type: String,
    #[serde(rename = "@name")]
    pub name: String,
}

#[derive(Debug, Serialize, Deserialize, PartialEq)]
#[serde(rename = "type")]
#[serde(rename_all = "lowercase")]
pub enum DiskType {
    File,
    Block,
    Dir,
    Network,
    Volume,
    NVME,
    VHostUser,
    VHostVDPA,
}

#[derive(Debug, Serialize, Deserialize, PartialEq)]
#[serde(rename_all = "camelCase")]
pub struct DomainDisk {
    #[serde(rename = "@type")]
    pub r#type: DiskType,
    #[serde(rename = "@device")]
    pub device: String,
    pub driver: DomainDiskDriver,
    pub source: DomainDiskFileSource,
}

#[derive(Debug, Serialize, Deserialize, PartialEq)]
#[serde(rename_all = "camelCase")]
pub struct DomainInterfaceMAC {
    pub address: String,
}

#[derive(Debug, Serialize, Deserialize, PartialEq)]
#[serde(rename = "type")]
#[serde(rename_all = "lowercase")]
pub enum DomainInterfaceModelType {}

#[derive(Debug, Serialize, Deserialize, PartialEq)]
#[serde(rename_all = "camelCase")]
pub struct DomainInterfaceModel {
    #[serde(rename = "@type")]
    pub r#type: String,
}

#[derive(Debug, Serialize, Deserialize, PartialEq)]
#[serde(rename = "type")]
#[serde(rename_all = "lowercase")]
pub enum DomainInterfaceType {
    Virtio,
    VirtioTransitional,
    VirtioNonTransitional,
    E1000,
    E1000E,
    IBG,
    RTL8139,
    Netfront,
    USBNet,
    // spapr-vlan,
    // lan9118,
    // scm91c111,
    // vlance,
    // vmxnet,
    // vmxnet2,
    // vmxnet3,
    // Am79C970A,
    // Am79C973,
    // 82540EM,
    // 82545EM,
    // 82543GC
}

#[derive(Debug, Serialize, Deserialize, PartialEq)]
#[serde(rename_all = "camelCase")]
pub struct DomainInterface {
    #[serde(rename = "@type")]
    pub r#type: DomainInterfaceType,
    #[serde(rename = "MAC")]
    pub mac: String,
    pub model: DomainInterfaceModel,
}

#[derive(Debug, Serialize, Deserialize, PartialEq)]
#[serde(rename_all = "camelCase")]
pub struct DomainChardev {
    #[serde(rename = "@type")]
    pub r#type: String,
}

#[derive(Debug, Serialize, Deserialize, PartialEq)]
#[serde(rename_all = "camelCase")]
pub struct DomainInput {
    #[serde(rename = "@type")]
    pub r#type: String,
    #[serde(rename = "@bus")]
    pub bus: String,
}

#[derive(Debug, Serialize, Deserialize, PartialEq)]
#[serde(rename_all = "camelCase")]
pub struct DomainGraphic {
    #[serde(rename = "@type")]
    pub r#type: String,
}

#[derive(Debug, Serialize, Deserialize, PartialEq)]
#[serde(rename = "model")]
#[serde(rename_all = "lowercase")]
pub enum VideoModelType {
    VGA,
    Cirrus,
    VMVGA,
    Xen,
    VBox,
    QXL,
    Virtio,
    GOP,
    Bochs,
    Ramfb,
    None,
}

#[derive(Debug, Serialize, Deserialize, PartialEq)]
#[serde(rename_all = "camelCase")]
pub struct DomainVideoModel {
    #[serde(rename = "@type")]
    pub r#type: VideoModelType,
}

#[derive(Debug, Serialize, Deserialize, PartialEq)]
#[serde(rename_all = "camelCase")]
pub struct DomainVideo {
    pub model: DomainVideoModel,
}

#[derive(Debug, Serialize, Deserialize, PartialEq)]
#[serde(rename_all = "camelCase")]
pub struct DomainDeviceList {
    pub controllers: Vec<DomainController>,
    #[serde(rename = "disk")]
    pub disks: Vec<DomainDisk>,
    #[serde(rename = "interface")]
    pub interfaces: Vec<DomainInterface>,
    #[serde(rename = "serial")]
    pub serials: Vec<DomainChardev>,
    #[serde(rename = "console")]
    pub consoles: Vec<DomainChardev>,
    #[serde(rename = "input")]
    pub inputs: Vec<DomainInput>,
    pub graphics: Vec<DomainGraphic>,
    #[serde(rename = "video")]
    pub videos: Vec<DomainVideo>,
}

#[skip_serializing_none]
#[derive(Debug, Serialize, Deserialize, PartialEq)]
#[serde(rename_all = "camelCase")]
pub struct DomainMemory {
    #[serde(rename = "$value")]
    pub value: String,
    pub unit: String,
}

#[derive(Debug, Serialize, Deserialize, PartialEq)]
#[serde(rename = "type")]
#[serde(rename_all = "lowercase")]
pub enum DomainType {
    Xen,
    KVM,
    HVF,
    QEMU,
    LXC,
}

impl DomainType {
    pub fn as_str(&self) -> &'static str {
        match self {
            DomainType::Xen => "xen",
            DomainType::KVM => "kvm",
            DomainType::HVF => "hvf",
            DomainType::QEMU => "qemu",
            DomainType::LXC => "lxc",
        }
    }
}

#[skip_serializing_none]
#[derive(Debug, Serialize, Deserialize, PartialEq)]
#[serde(rename = "domain")]
#[serde(rename_all = "camelCase")]
pub struct Domain {
    #[serde(rename = "@type")]
    pub r#type: DomainType,
    pub name: String,
    pub uuid: Option<String>,
    pub memory: Option<DomainMemory>,
    pub current_memory: Option<DomainMemory>,
    pub devices: Option<DomainDeviceList>,
}

#[cfg(test)]
mod tests {
    use indoc::indoc;
    use crate::domain::*;
    use quick_xml::se::Serializer;
    use anyhow::Result;
    use pretty_assertions::assert_eq;

    fn to_pretty_string(data: impl Serialize) -> Result<String> {
        let mut buffer = String::new();
        let mut ser = Serializer::new(&mut buffer);
        ser.indent(' ', 2);

        data.serialize(ser)?;
        Ok(buffer)
    }

    #[test]
    fn simple() {
        // TODO(blugo): Is there a way to make these Nones more ergonomic?
        let object = Domain {
            r#type: DomainType::KVM,
            name: "test".to_string(),
            uuid: None,
            memory: None,
            current_memory: None,
            devices: None,
        };

        let expected = indoc! {r#"
            <domain type="kvm">
              <name>test</name>
            </domain>
        "#}.trim_end();

        assert_eq!(to_pretty_string(&object).unwrap(), expected);
    }

    #[test]
    fn disk() {
        let object = Domain {
            r#type: DomainType::KVM,
            name: "test".to_string(),
            uuid: None,
            memory: None,
            current_memory: None,
            devices: Option::from(DomainDeviceList {
                controllers: vec![],
                disks: vec![
                    DomainDisk {
                    r#type: DiskType::File,
                    device: "cdrom".to_string(),
                    driver: DomainDiskDriver { name: "qemu".to_string(), r#type: "qcow2".to_string() },
                    source: DomainDiskFileSource { file: "/var/lib/libvirt/images/demo.qcow2".to_string() },
                }],
                interfaces: vec![],
                serials: vec![],
                consoles: vec![],
                inputs: vec![],
                graphics: vec![],
                videos: vec![],
            }),
        };

        let expected = indoc! {r#"
            <domain type="kvm">
              <name>test</name>
              <devices>
                <disk type="file" device="cdrom">
                  <driver name="qemu" type="qcow2"/>
                  <source file="/var/lib/libvirt/images/demo.qcow2"/>
                </disk>
              </devices>
            </domain>
        "#}.trim_end();

        assert_eq!(to_pretty_string(&object).unwrap(), expected);
    }

    #[test]
    fn device_list() {
        let object = Domain {
            r#type: DomainType::KVM,
            name: "test".to_string(),
            uuid: None,
            memory: None,
            current_memory: None,
            devices: Option::from(DomainDeviceList {
                controllers: vec![],
                disks: vec![],
                interfaces: vec![],
                serials: vec![],
                consoles: vec![],
                inputs: vec![
                    DomainInput {
                        r#type: "tablet".to_string(),
                        bus: "usb".to_string(),
                    },
                    DomainInput {
                        r#type: "keyboard".to_string(),
                        bus: "ps2".to_string(),
                    }
                ],
                graphics: vec![
                    DomainGraphic {
                        r#type: "vnc".to_string(),
                    }
                ],
                videos: vec![
                    DomainVideo {
                        model: DomainVideoModel { r#type: VideoModelType::Cirrus },
                    }
                ],
            }),
        };

        let expected = indoc! {r#"
            <domain type="kvm">
              <name>test</name>
              <devices>
                <input type="tablet" bus="usb"/>
                <input type="keyboard" bus="ps2"/>
                <graphics type="vnc"/>
                <video>
                  <model type="cirrus"/>
                </video>
              </devices>
            </domain>
        "#}.trim_end();

        assert_eq!(to_pretty_string(&object).unwrap(), expected);
    }
}
